require 'rails_helper'

RSpec.describe Api::V1::InventoryItemTypesController, type: :controller do
  let(:valid_attributes) { { name: 'Water', points: 4 } }

  let(:invalid_attributes) { { nome: 'Water', pontos: 4, name: '' } }

  let(:inventory_item_type) { create(:inventory_item_type) }

  describe 'GET #index' do
    it 'returns a success response' do
      get :index, params: {}

      expect(response).to be_success
      expect(json.any?).to be(true)
      expect(json.first.key?('name')).to be(true)
      expect(json.first.key?('points')).to be(true)
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      get :show, params: { id: inventory_item_type.to_param }
      expect(response).to be_success
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new InventoryItemType' do
        expect do
          post :create, params: { inventory_item_type: valid_attributes }
        end.to change(InventoryItemType, :count).by(1)
      end

      it 'renders a JSON response with the new inventory_item_type' do
        post :create, params: { inventory_item_type: valid_attributes }

        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json')
        expect(json.key?('name')).to be(true)
        expect(json.key?('points')).to be(true)
      end
    end

    context 'with invalid params' do
      it 'renders a JSON response with errors for the new inventory_item_type' do
        post :create, params: { inventory_item_type: invalid_attributes }

        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) { { name: 'Elixir', points: 10 } }

      it 'updates the requested inventory_item_type' do
        put :update, params: { id: inventory_item_type.to_param, inventory_item_type: new_attributes }
        inventory_item_type.reload

        expect(inventory_item_type.name).to eq new_attributes[:name]
        expect(inventory_item_type.points).to eq new_attributes[:points]
      end

      it 'renders a JSON response with the inventory_item_type' do
        put :update, params: { id: inventory_item_type.to_param, inventory_item_type: valid_attributes }

        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json')
      end
    end

    context 'with invalid params' do
      it 'renders a JSON response with errors for the inventory_item_type' do
        put :update, params: { id: inventory_item_type.to_param, inventory_item_type: invalid_attributes }

        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested inventory_item_type' do
      item_type = inventory_item_type

      expect do
        delete :destroy, params: { id: item_type.to_param }
      end.to change(InventoryItemType, :count).by(-1)
    end
  end
end
