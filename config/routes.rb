Rails.application.routes.draw do
  scope module: 'api' do
    namespace :v1 do
      resources :survivors, only: %i[show create] do
        patch 'location', to: 'survivors#update_location'
        post 'report/infected', to: 'survivors#report_as_infected'

        post 'inventory_items', to: 'inventory_items#create'
        post 'inventory_items/trade/:trade_with_survivor_id', to: 'inventory_items#trade'
      end

      resources :inventory_item_types

      get 'reports/percentage_infected_survivors', to: 'reports#percentage_infected_survivors'
      get 'reports/percentage_non_infected_survivors', to: 'reports#percentage_non_infected_survivors'
      get 'reports/average_amount_of_resource_per_survivor', to: 'reports#average_amount_of_resource_per_survivor'
      get 'reports/points_lost_because_of_infected_survivors', to: 'reports#points_lost_because_of_infected_survivors'
    end
  end
end
