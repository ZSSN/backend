require 'rails_helper'

RSpec.describe InventoryItemType, type: :model do
  %i[name points].each do |attribute|
    it { should validate_presence_of(attribute) }
  end

  it { should have_many(:inventory_items) }
end
