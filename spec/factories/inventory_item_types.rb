FactoryBot.define do
  factory :inventory_item_type do
    name 'Water'
    points 4
  end
end
