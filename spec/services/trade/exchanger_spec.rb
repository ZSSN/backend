require 'rails_helper'

RSpec.describe Trade::Exchanger do
  before(:each) do
    @water = InventoryItemType.find_by(name: 'Water')
    @medication = InventoryItemType.find_by(name: 'Medication')

    @survivor = create(:survivor)
    @inventory_item = create(:inventory_item, survivor: @survivor, inventory_item_type: @water, quantity: 1)

    @trade_with_survivor = create(:survivor)
    @trade_with_inventory_item = create(:inventory_item, survivor: @trade_with_survivor, inventory_item_type: @medication, quantity: 5)
  end

  let(:valid_offer) { Trade::Offer.new(@survivor.id, [{ type_id: @water.id, quantity: 1 }]) }
  let(:valid_trade_with_offer) { Trade::Offer.new(@trade_with_survivor.id, [{ type_id: @medication.id, quantity: 2 }]) }
  let(:invalid_trade_with_offer) { Trade::Offer.new(@trade_with_survivor.id, [{ type_id: @medication.id, quantity: 5 }]) }

  let(:valid_exchanger) { described_class.new(valid_offer, valid_trade_with_offer) }
  let(:invalid_exchanger) { described_class.new(valid_offer, invalid_trade_with_offer) }

  it 'has to raise ArgumentError if initialize without Offer objects' do
    expect { described_class.new('test', 'test') }.to raise_error(ArgumentError)
    expect { described_class.new(valid_offer, 'test') }.to raise_error(ArgumentError)
    expect { described_class.new('test', valid_offer) }.to raise_error(ArgumentError)
  end

  it 'has to initialize correctly if it has two valid offers' do
    expect { valid_exchanger }.not_to raise_error
  end

  describe 'trade!' do
    it 'has to trade with two survivors offers' do
      valid_exchanger.trade!

      survivor_inventory_items = InventoryItem.where(survivor: @survivor, type: @medication)
      expect(survivor_inventory_items.any?).to be(true)

      trade_with_survivor_inventory_items = InventoryItem.where(survivor: @trade_with_survivor, type: @water)
      expect(trade_with_survivor_inventory_items.any?).to be(true)
    end

    it 'has to raise Error::OffersPointsDifferent if offers has different points' do
      expect { invalid_exchanger.trade! }.to raise_error(Error::OffersPointsDifferent)
    end
  end
end
