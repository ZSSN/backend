module Api
  module V1
    class InventoryItemsController < ActionController::API
      include Error::ErrorHandler

      # POST /survivors/:survivor_id/inventory_items
      def create
        check_survivor_hasnt_inventory!

        inventory_items_params.each do |inventory_item|
          InventoryItem.create! inventory_item.merge(survivor: @survivor)
        end

        render json: inventory_items_params, status: :created
      end

      # POST /survivors/:survivor_id/inventory_items/trade/:trade_with_survivor_id
      def trade
        survivor_id = params[:survivor_id]
        inventory_items = trade_inventory_items_params[:inventory_items]
        trade_with_survivor_id = params[:trade_with_survivor_id]
        trade_with_inventory_items = trade_inventory_items_params[:trade_with_inventory_items]

        offer = Trade::Offer.new(survivor_id, inventory_items)
        trade_with_offer = Trade::Offer.new(trade_with_survivor_id, trade_with_inventory_items)

        exchanger = Trade::Exchanger.new(offer, trade_with_offer)
        exchanger.trade!

        render json: { message: 'successful trade' }
      end

      private

      def check_survivor_hasnt_inventory!
        @survivor = Survivor.find(params[:survivor_id])

        return if @survivor.inventory_items.empty?

        raise Error::SurvivorAlreadyHasInventoryError
      end

      def inventory_items_params
        unless params.fetch(:inventory_items).is_a?(Array)
          raise ArgumentError, 'You have to pass a valid array of :inventory_items'
        end

        params.permit(inventory_items: %i[inventory_item_type_id quantity]).require(:inventory_items)
      end

      def trade_inventory_items_params
        params.require(:trade).permit(inventory_items: [:type_id, :quantity], trade_with_inventory_items: [:type_id, :quantity])
      end
    end
  end
end
