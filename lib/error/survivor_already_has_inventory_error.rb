module Error
  class SurvivorAlreadyHasInventoryError < StandardError
    def initialize(message = 'Survivor already has an inventory')
      super
    end
  end
end
