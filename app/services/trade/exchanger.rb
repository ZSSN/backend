module Trade
  class Exchanger
    def initialize(offer, trade_with_offer)
      raise ArgumentError unless offer.is_a?(Offer) &&
                                 trade_with_offer.is_a?(Offer)

      @offer = offer
      @trade_with_offer = trade_with_offer
    end

    def trade!
      check_offers_points_are_different!
      @offer.trade_with! @trade_with_offer
    end

    private

    def check_offers_points_are_different!
      return if offers_are_equal?
      raise Error::OffersPointsDifferent.new(diff: diff_between_offers)
    end

    def diff_between_offers
      @offer - @trade_with_offer
    end

    def offers_are_equal?
      @offer == @trade_with_offer
    end
  end
end
