# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

inventory_item_types = [
  { name: 'Water', points: '4' },
  { name: 'Food', points: '3' },
  { name: 'Medication', points: '2' },
  { name: 'Ammunition', points: '1' },
]

inventory_item_types.each { |item| InventoryItemType.find_or_create_by item }
