class InventoryItem < ApplicationRecord
  belongs_to :survivor
  belongs_to :inventory_item_type

  alias_attribute :type, :inventory_item_type

  validates :survivor, :inventory_item_type, :quantity, presence: true

  class << self
    def find_available_inventory_item(survivor, type, quantity)
      inventory_item = where(survivor: survivor)
                       .where(type: type)
                       .where('quantity >= ?', quantity)
                       .take!

      inventory_item.quantity = quantity
      inventory_item
    end

    def assign_to_survivor(survivor, inventory_items)
      inventory_items.each do |inventory_item|
        decrement_inventory_item(inventory_item)

        inventory_item.survivor = survivor
        increment_inventory_item(inventory_item)
      end
    end

    def average_amount_of_resource_per_survivor
      survivors_number = Survivor.not_infected.count

      non_infected_survivors_inventory_items_per_types.map do |type, inventory_items|
        {
          inventory_item_type: type,
          average_per_user: sum_amount(inventory_items) / survivors_number.to_f
        }
      end
    end

    def points_lost_because_of_infected_survivors
      infected_survivors_inventory_items.inject(0) do |amount, inventory_item|
        amount + (inventory_item.type.points * inventory_item.quantity)
      end
    end

    private

    def sum_amount(inventory_items)
      inventory_items.inject(0) do |amount, inventory_item|
        amount + inventory_item.quantity
      end
    end

    def non_infected_survivors_inventory_items_per_types
      survivors_inventory_items
        .where('survivors.infected = ?', false)
        .group_by(&:type)
    end

    def infected_survivors_inventory_items
      survivors_inventory_items
        .where('survivors.infected = ?', true)
    end

    def survivors_inventory_items
      includes(:inventory_item_type)
        .joins(:survivor)
        .all
    end

    def decrement_inventory_item(inventory_item)
      finded_inventory_item = find_by(
        survivor: inventory_item.survivor,
        type: inventory_item.type
      )

      finded_inventory_item.quantity -= inventory_item.quantity
      finded_inventory_item.save!
    end

    def increment_inventory_item(inventory_item)
      finded_inventory_item = find_or_initialize_by(
        survivor: inventory_item.survivor,
        type: inventory_item.type
      )

      finded_inventory_item.quantity ||= 0

      finded_inventory_item.quantity += inventory_item.quantity
      finded_inventory_item.save!
    end
  end

  def save!
    return destroy if quantity <= 0
    super
  end
end
