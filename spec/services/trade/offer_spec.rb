require 'rails_helper'

RSpec.describe Trade::Offer do
  before(:each) do
    @water = InventoryItemType.find_by(name: 'Water')
    @medication = InventoryItemType.find_by(name: 'Medication')

    @survivor = create(:survivor)
    @inventory_item = create(:inventory_item, survivor: @survivor, inventory_item_type: @water, quantity: 1)

    @trade_with_survivor = create(:survivor)
    @trade_with_inventory_item = create(:inventory_item, survivor: @trade_with_survivor, inventory_item_type: @medication, quantity: 5)
  end

  let(:infected_survivor) { create(:survivor, reports_as_infected: 3, infected: true) }

  let(:valid_offer) { described_class.new(@survivor.id, [{ type_id: @water.id, quantity: 1 }]) }
  let(:valid_trade_with_offer) { described_class.new(@trade_with_survivor.id, [{ type_id: @medication.id, quantity: 2 }]) }
  let(:invalid_trade_with_offer) { described_class.new(@trade_with_survivor.id, [{ type_id: @medication.id, quantity: 5 }]) }

  it 'has to raise ActiveRecord::NotFound if survivor_id not founded' do
    expect { described_class.new(99, nil) }.to raise_error(ActiveRecord::RecordNotFound)
  end

  it 'has to raise ActiveRecord::NotFound if inventory_items are not available (like trying to offer more than survivor have)' do
    expect do
      described_class.new(@survivor.id, [{ type_id: @water.id, quantity: 2 }])
    end.to raise_error(ActiveRecord::RecordNotFound)
  end

  it 'has to raise Error::SurvivorInfectedError if survivor_id not founded' do
    expect { described_class.new(infected_survivor.id, nil) }.to raise_error(Error::SurvivorInfectedError)
  end

  describe '.sum_of_points' do
    it 'sum correctly multiple inventory_items with any quantity' do
      offers = [valid_offer, valid_trade_with_offer, invalid_trade_with_offer]

      offers.each do |offer|
        sum = offer.inventory_items.inject(0) do |sum_points, inventory_item|
          sum_points + (inventory_item.type.points * inventory_item.quantity)
        end

        expect(offer.sum_of_points).to be(sum)
      end
    end
  end

  describe '- (subtraction)' do
    it 'has to result in zero if we subtract two equal offers (with the same number of points)' do
      expect(valid_offer - valid_trade_with_offer).to be(0)
    end

    it 'has to result different zero if we subtract two different offers' do
      expect(valid_offer - invalid_trade_with_offer).not_to be(0)
    end
  end

  describe '==' do
    it 'has to result in true if we check two offers with same number of points' do
      expect(valid_offer.sum_of_points == valid_trade_with_offer.sum_of_points).to be(true)
      expect(valid_offer == valid_trade_with_offer).to be(true)
    end
  end
end
