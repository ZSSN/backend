FactoryBot.define do
  factory :survivor do
    name 'Rafael'
    age '25'
    gender 'Male'
  end
end