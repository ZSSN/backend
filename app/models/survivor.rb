class Survivor < ApplicationRecord
  has_many :inventory_items

  NUMBER_REPORTS_TO_BE_INFECTED = 3

  validates :name, :age, :gender, presence: true
  validates :latitude, :longitude, numericality: true, allow_nil: true

  after_initialize :default_values

  def self.infected
    where(infected: true)
  end

  def self.not_infected
    where(infected: false)
  end

  def report_as_infected
    self.reports_as_infected += 1
    self.infected = infected?
  end

  def infected?
    self.reports_as_infected >= NUMBER_REPORTS_TO_BE_INFECTED
  end

  private

  def default_values
    self.reports_as_infected ||= 0
    self.infected ||= false
  end
end
