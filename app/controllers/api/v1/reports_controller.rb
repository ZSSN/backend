module Api
  module V1
    class ReportsController < ActionController::API
      include Error::ErrorHandler

      def percentage_infected_survivors
        percentage = Survivor.infected.count / Survivor.count.to_f
        render json: { percentage: percentage }
      end

      def percentage_non_infected_survivors
        percentage = Survivor.not_infected.count / Survivor.count.to_f
        render json: { percentage: percentage }
      end

      def average_amount_of_resource_per_survivor
        render json: InventoryItem.average_amount_of_resource_per_survivor
      end

      def points_lost_because_of_infected_survivors
        render json: { points_lost: InventoryItem.points_lost_because_of_infected_survivors }
      end
    end
  end
end
