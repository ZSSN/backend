class AddColumnQuantityToInventoryItem < ActiveRecord::Migration[5.1]
  def change
    add_column :inventory_items, :quantity, :integer
  end
end
