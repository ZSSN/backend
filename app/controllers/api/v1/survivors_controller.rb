module Api
  module V1
    class SurvivorsController < ActionController::API
      include Error::ErrorHandler

      before_action :set_survivor, only: %i[update_location report_as_infected]

      # POST /survivors
      def create
        @survivor = Survivor.create!(survivor_params)
        render json: @survivor, status: :created
      end

      # PATCH /survivors/:survivor_id/location
      def update_location
        @survivor.update!(location_params)
        render json: @survivor, status: :ok
      end

      # POST /survivors/:survivor_id/report/infected
      def report_as_infected
        @survivor.report_as_infected
        @survivor.save!
        render json: @survivor, status: :ok
      end

      private

      def set_survivor
        @survivor = Survivor.find(params[:survivor_id])
      end

      def survivor_params
        params.require(:survivor).permit(:name, :age, :gender)
      end

      def location_params
        params.require(:location).permit(:latitude, :longitude)
      end
    end
  end
end
