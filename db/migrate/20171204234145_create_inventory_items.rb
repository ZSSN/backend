class CreateInventoryItems < ActiveRecord::Migration[5.1]
  def change
    create_table :inventory_items do |t|
      t.belongs_to :inventory_item_type, index: true, foreign_key: true
      t.belongs_to :survivor, index: true, foreign_key: true
      
      t.timestamps
    end
  end
end
