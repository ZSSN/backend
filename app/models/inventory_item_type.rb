class InventoryItemType < ApplicationRecord
  has_many :inventory_items

  validates :name, :points, presence: true
end
