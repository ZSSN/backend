require 'rails_helper'

RSpec.describe Api::V1::SurvivorsController, type: :controller do
  let(:survivor) { create(:survivor) }

  let(:survivor_almost_infected) do
    create(:survivor, reports_as_infected: Survivor::NUMBER_REPORTS_TO_BE_INFECTED - 1 )
  end

  let(:coordinates) { { latitude: -22.8503463, longitude: -47.0741068 } }

  describe 'POST create' do
    it 'creates a survivor' do
      post :create, params: { survivor: { name: 'Rafael', age: 25, gender: 'Male' } }

      expect(response).to have_http_status(:created)
      expect(json).to have_key('id')
    end
  end

  describe 'PATCH update_location' do
    it 'updates a survivor location when survivor is founded' do
      patch :update_location, params: { survivor_id: survivor.id, location: coordinates }

      expect(response).to have_http_status(:ok)
      expect(json['latitude']).to eq coordinates[:latitude].to_s
    end

    it 'returns 404 if survivor is not founded' do
      patch :update_location, params: { survivor_id: 123, location: coordinates }

      expect(response).to have_http_status(:not_found)
    end
  end

  describe 'POST report_as_infected' do
    it 'reports as infected a survivor' do
      post :report_as_infected, params: { survivor_id: survivor.id }

      expect(response).to have_http_status(:ok)
      expect(json['reports_as_infected']).to eq 1
    end

    it 'marks survivor as infected if its third time' do
      post :report_as_infected, params: { survivor_id: survivor_almost_infected.id }

      expect(response).to have_http_status(:ok)
      expect(json['reports_as_infected']).to eq 3
      expect(json['infected']).to be true
    end

    it 'returns 404 if survivor is not founded' do
      post :report_as_infected, params: { survivor_id: 123 }

      expect(response).to have_http_status(:not_found)
    end
  end
end
