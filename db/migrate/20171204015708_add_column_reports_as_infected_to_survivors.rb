class AddColumnReportsAsInfectedToSurvivors < ActiveRecord::Migration[5.1]
  def change
    add_column :survivors, :reports_as_infected, :integer    
    add_column :survivors, :infected, :boolean
  end
end
