require 'rails_helper'

RSpec.describe Survivor, type: :model do
  %i[name age gender].each do |attribute|
    it { should validate_presence_of(attribute) }
  end

  it { should validate_numericality_of(:latitude).allow_nil }
  it { should validate_numericality_of(:longitude).allow_nil }

  it { should have_many(:inventory_items) }

  let(:survivor) { create(:survivor) }

  describe '.report_as_infected' do
    it 'has to increment by 1 the number of reports_as_infected' do
      survivor.report_as_infected
      expect(survivor.reports_as_infected).to be(1)
    end

    it 'has to mark as infected if survivor had more than three reports' do
      3.times { survivor.report_as_infected }
      expect(survivor.reports_as_infected).to be(3)
      expect(survivor.infected?).to be(true)
    end
  end
end
