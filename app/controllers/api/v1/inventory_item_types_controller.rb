module Api
  module V1
    class InventoryItemTypesController < ApplicationController
      include Error::ErrorHandler

      before_action :set_inventory_item_type, only: %i[show update destroy]

      # GET /inventory_item_types
      def index
        @inventory_item_types = InventoryItemType.all

        render json: @inventory_item_types
      end

      # GET /inventory_item_types/1
      def show
        render json: @inventory_item_type
      end

      # POST /inventory_item_types
      def create
        @inventory_item_type = InventoryItemType.create!(inventory_item_type_params)
        render json: @inventory_item_type, status: :created
      end

      # PATCH/PUT /inventory_item_types/1
      def update
        @inventory_item_type.update!(inventory_item_type_params)
        render json: @inventory_item_type
      end

      # DELETE /inventory_item_types/1
      def destroy
        @inventory_item_type.destroy
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_inventory_item_type
        @inventory_item_type = InventoryItemType.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def inventory_item_type_params
        params.require(:inventory_item_type).permit(:name, :points)
      end
    end
  end
end
