module Error
  class SurvivorInfectedError < StandardError
    def initialize(survivor)
      super "'#{survivor.name}' is infected, infected survivors can't trade their items"
    end
  end
end
