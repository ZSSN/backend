require 'rails_helper'

RSpec.describe InventoryItem, type: :model do
  it { should belong_to(:survivor) }
  it { should belong_to(:inventory_item_type) }

  before(:each) do
    @survivor = create(:survivor)
    @water = InventoryItemType.find_by(name: 'Water')
    @medication = InventoryItemType.find_by(name: 'Medication')

    @inventory_item = create(:inventory_item, survivor: @survivor, inventory_item_type: @water, quantity: 1)
    @medication_item = create(:inventory_item, survivor: @survivor, inventory_item_type: @medication, quantity: 2)

    @trade_with_survivor = create(:survivor)
    @trade_with_inventory_item = create(:inventory_item, survivor: @trade_with_survivor, inventory_item_type: @medication, quantity: 5)
  end

  describe '.find_available_inventory_item(survivor, type, quantity)' do
    it 'has to return the first available inventory_item from survivor with type and quantity' do
      finded_inventory_item = described_class.find_available_inventory_item(@survivor, @water, 1)

      expect(finded_inventory_item.type == @water).to be(true)
      expect(finded_inventory_item.survivor == @survivor).to be(true)
      expect(finded_inventory_item.quantity).to be(1)
    end

    it 'has to raise exception if not find available inventory_item from survivor with type and quantity' do
      expect do
        described_class.find_available_inventory_item(@survivor, @water, 5)
      end.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  describe '.assign_to_survivor(survivor, inventory_items)' do
    it 'has to assign inventory_items to survivor' do
      described_class.assign_to_survivor(@survivor, [@trade_with_inventory_item])

      # inventory_item removed from his last owner
      expect { @trade_with_inventory_item.reload }.to raise_error(ActiveRecord::RecordNotFound)

      inventory_item = InventoryItem.where(survivor: @survivor, type: @trade_with_inventory_item.type).take!
      expect(inventory_item.quantity).to be(@medication_item.quantity + @trade_with_inventory_item.quantity)
    end
  end

  describe '.save!' do
    it 'it will destroy himself if quantity <= 0' do
      @inventory_item.quantity = 0
      @inventory_item.save!
      expect { @inventory_item.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
