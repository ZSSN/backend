module Trade
  class Offer
    attr_reader :survivor, :inventory_items

    def initialize(survivor_id, inventory_items)
      @survivor = Survivor.find(survivor_id)

      raise Error::SurvivorInfectedError.new(@survivor) if @survivor.infected?

      @inventory_items = find_available_inventory_items(inventory_items)
    end

    def trade_with!(other)
      raise ArgumentError, 'This argument should be an Trade::Offer' unless other.is_a?(Offer)

      InventoryItem.transaction do
        InventoryItem.assign_to_survivor(other.survivor, @inventory_items)
        InventoryItem.assign_to_survivor(@survivor, other.inventory_items)
      end
    end

    def ==(other)
      return false unless other.is_a?(Offer)

      sum_of_points == other.sum_of_points
    end

    def -(other)
      raise ArgumentError, 'This argument should respond_to :sum_of_points' unless other.is_a?(Offer)

      sum_of_points - other.sum_of_points
    end

    def sum_of_points
      @inventory_items.inject(0) do |sum_points, inventory_item|
        sum_points + (inventory_item.type.points * inventory_item.quantity)
      end
    end

    private

    def find_available_inventory_items(inventory_items)
      inventory_items.map do |inventory_item|
        InventoryItem.find_available_inventory_item(@survivor, inventory_item[:type_id], Integer(inventory_item[:quantity]))
      end
    end
  end
end
