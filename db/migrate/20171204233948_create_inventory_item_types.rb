class CreateInventoryItemTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :inventory_item_types do |t|
      t.string :name
      t.integer :points
      
      t.timestamps
    end
  end
end
