require 'rails_helper'

RSpec.describe Api::V1::InventoryItemsController, type: :controller do
  let(:valid_inventory_items) do
    [
      { inventory_item_type_id: 2, quantity: 3 },
      { inventory_item_type_id: 1, quantity: 4 }
    ]
  end

  let(:invalid_attributes) do
    [
      { name: 'wrong name', quantity: 3 },
      { points: 10, inventory_item_type_name: 'Water' }
    ]
  end

  let(:survivor) { create(:survivor) }

  describe 'POST #create' do
    context 'with valid params' do
      it 'register new InventoryItem to survivor' do
        expect do
          post :create, params: { survivor_id: survivor.id, inventory_items: valid_inventory_items }
        end.to change(InventoryItem, :count).by(valid_inventory_items.length)
      end

      it 'return locked code if its the second try' do
        2.times do
          post :create, params: { survivor_id: survivor.id, inventory_items: valid_inventory_items }
        end

        expect(response).to have_http_status(:locked)
      end
    end

    context 'with invalid params' do
      it 'renders a JSON response with errors for the new inventory_item' do
        post :create, params: { survivor_id: survivor.id, inventory_items: invalid_attributes }

        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end

  describe 'POST #trade' do
    let(:survivors) do
      [create(:survivor), create(:survivor)]
    end

    let(:type) { InventoryItemType.find(1) }
    let(:trade_with_type) { InventoryItemType.find(3) }

    let(:inventory_item) { create(:inventory_item, type: type, survivor: survivors.first, quantity: 1) }
    let(:trade_with_inventory_item) { create(:inventory_item, type: trade_with_type, survivor: survivors.second, quantity: 2) }

    let(:valid_trade) do
      {
        inventory_items: [
          { type_id: inventory_item.type.id, quantity: 1 }
        ],
        trade_with_inventory_items: [
          { type_id: trade_with_inventory_item.type.id, quantity: 2 }
        ]
      }
    end

    let(:invalid_trade) do
      {
        inventory_items: [
          { type_id: inventory_item.type.id, quantity: 99 }
        ],
        trade_with_inventory_items: [
          { type_id: trade_with_inventory_item.type.id, quantity: 2 }
        ]
      }
    end

    context 'with valid params' do
      it 'trades inventory items between survivors' do
        post :trade, params: { survivor_id: survivors.first.id, trade_with_survivor_id: survivors.second.id, trade: valid_trade }

        expect(response).to have_http_status(:ok)
        expect { inventory_item.reload }.to raise_error(ActiveRecord::RecordNotFound)
        expect { trade_with_inventory_item.reload }.to raise_error(ActiveRecord::RecordNotFound)

        expect(InventoryItem.find_by(survivor: survivors.first).type == trade_with_type).to be(true)
        expect(InventoryItem.find_by(survivor: survivors.second).type == type).to be(true)
      end
    end

    context 'with invalid params' do
      it 'expects to response with errors status' do
        post :trade, params: { survivor_id: survivors.first.id, trade_with_survivor_id: survivors.second.id, trade: invalid_trade }

        expect(response).to have_http_status(:not_found)
        expect(json['message'].start_with?('Couldn\'t find')).to be(true)
      end
    end

    context 'with invalid survivors ids' do
      it 'expects to response with errors status if its the first survivor id is wrong' do
        post :trade, params: { survivor_id: 99, trade_with_survivor_id: survivors.second.id, trade: valid_trade }

        expect(response).to have_http_status(:not_found)
        expect(json['message'].start_with?('Couldn\'t find')).to be(true)
      end

      it 'expects to response with errors status if its the second survivor id is wrong' do
        post :trade, params: { survivor_id: survivors.first.id, trade_with_survivor_id: 99, trade: valid_trade }

        expect(response).to have_http_status(:not_found)
        expect(json['message'].start_with?('Couldn\'t find')).to be(true)
      end
    end
  end
end
