module Error
  class OffersPointsDifferent < StandardError
    def initialize(args)
      message = args.fetch(:message, 'Exchange items points differ')
      diff = args.fetch(:diff)

      super "#{message} by #{diff}" unless diff.nil?
    end
  end
end
