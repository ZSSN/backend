module Error
  module ErrorHandler
    def self.included(clazz)
      clazz.class_eval do
        rescue_from StandardError, with: :standard
        rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
        rescue_from ActiveRecord::RecordInvalid, with: :record_invalid
        rescue_from Error::SurvivorAlreadyHasInventoryError, with: :survivor_already_has_inventory
        rescue_from Error::OffersPointsDifferent, with: :offers_points_different
      end
    end

    private

    def standard(exception)
      render json: { message: exception.message }, status: :internal_server_error
    end

    def record_not_found(exception)
      render json: { message: exception.message }, status: :not_found
    end

    def record_invalid(exception)
      render json: exception.record.errors, status: :unprocessable_entity
    end

    def survivor_already_has_inventory(exception)
      render json: { message: exception.message }, status: :locked
    end

    def offers_points_different(exception)
      render json: { message: exception.message }, status: :locked
    end
  end
end
